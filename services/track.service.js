const { TrackDTO } = require('../dto/track.dto');
const db = require('../models')



const trackService = {
    getAll : async (offset, limit) => {

        const { rows, count } = await db.Track.findAndCountAll({
            distinct : true,
            offset,
            limit
            //TODO rajouter genre
            //TODO rajouter albums
            //TODO rajouter artists
        });

        return {
            tracks : rows.map(track => new TrackDTO(track)),
            count
        } 
    },

    getById : async (id) => {
        const track = await db.Track.findByPk(id, {
            //TODO rajouter genre
            //TODO rajouter albums
            //TODO rajouter artists
        }); 

        return track ? new TrackDTO(track) : null; 
        //Si le genre n'est pas undefined, on renvoie le DTO, sinon, on envoie null
    },

    create : async (trackToAdd) => {
        //TODO créer une transaction pour pouvoir faire plusieurs actions db et rollback si pb

        const track = await db.Track.create(trackToAdd);
        //TODO ajouter liens artists
        //TODO ajouter liens albums
        
        //Récupérer en db la track avec artists et albums

        return track ? new TrackDTO(track) : null;
    },

    update : async (id, trackToUpdate) => {
        const updatedRow = await db.Track.update(trackToUpdate, {
            where : { id }
        });
       
        return updatedRow[0] === 1; 
    },

    delete : async (id) => {
        const nbDeletedRow = await db.Track.destroy({
            where : { id }
        });

        return nbDeletedRow === 1; //Est-ce que nbrow supprimées = 1 ? si oui delete réussi, si non delete raté
    }
}


module.exports = trackService;